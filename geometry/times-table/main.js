/*
NOTE: If  line 55 and/or 56 are removed the display gets messed up

TODO: Add UI

*/

window.onload = () => {
	let canvas = document.getElementById("canvas")
	let ctx = canvas.getContext("2d")
	let width = canvas.width = window.innerWidth
	let height = canvas.height = window.innerHeight
	const {min, max, PI, sin, cos} = Math;
	const radius = min(width, height) / 3
	let settings = window.settings = {
		n: 100,
		m: 2,
		rate: 1
	}
	let points = []

	let gui = new dat.GUI()
  let rateC = gui.add(settings, "rate", [1, 0.1, 0.01]).listen()
  let nC = gui.add(settings, "n", 1, 250).listen()
  let mC = gui.add(settings, "m", 1, 300).listen()

  gui.open()

	rateC.onFinishChange(v => { render() })
	nC.onFinishChange(v => { render() })
	mC.onFinishChange(v => { render() })

	render()

	document.onkeypress  = (e) => {
		switch (e.keyCode) {
			case 13: // Enter
			case 32: // Spacebar
			case 109: // m
				settings.m += parseFloat(settings.rate)
				render()
				break
			case 110: // n
				settings.n++
				render()
				break
			case 106: //j
				settings.n--
				render()
				break
			case 107: // k
				settings.m -= parseFloat(settings.rate)
				render()
				break
		}
	}

	function round(num, dp) {
		return parseFloat(num.toFixed(dp))
	}

	function countDecimals(num) {
  		if (Math.floor(num) === num) return 0;
  		return num.toString().split(".")[1].length || 0;
	}

	function render() {
		canvas.width = canvas.width      // For some reason if i change one of these
		canvas.height = canvas.height    // the whole thing messes up, strange... isn't it?

		ctx.clearRect(-width/2, -height/2, width, height)

		ctx.translate(window.innerWidth/2, window.innerHeight/2)

		points = []

		ctx.font = "20px Arial"
		ctx.fillText(`Divisions [n] : ${settings.n}`, -width/2 + 20, -height/2 + 40)
		ctx.fillText(`Multiplier [m] : ${settings.m}`, -width/2 + 20, -height/2 + 70)
		ctx.fillText(`Change rate [rate] : ${settings.rate}`, -width/2 + 20, -height/2 + 100)

		points = []

		ctx.arc(0, 0, radius, 0, 2*PI)
		ctx.stroke()
		ctx.closePath()

		for(let i = 0; i <= settings.n; i++) {
			const x = radius * sin(i * ((PI*2)/settings.n))
			const y = radius * cos(i * ((PI*2)/settings.n))
			ctx.beginPath()
			ctx.arc(x, y, 2.5, 0, PI*2)
			ctx.fill()
			ctx.closePath()
			points[i] = {x, y}
		}

		for(let i = 0; i <= settings.n; i++) {
			ctx.beginPath()
			ctx.moveTo(points[i].x, points[i].y)
			const index = round(((i*settings.m) % settings.n), countDecimals(settings.m)) // Prevent decimals like 1.00001 or 1.999999 from occuring
			let point;
			if(points.hasOwnProperty(index)){
				point = points[index]
			} else {
				point = {
					x: radius * sin(index * ((PI*2)/settings.n)),
					y: radius * cos(index * ((PI*2)/settings.n))
				}
			}
			ctx.lineTo(point.x, point.y)
			ctx.stroke()
			ctx.closePath()
		}
	}
}
